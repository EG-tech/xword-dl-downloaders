# xword-dl-downloaders

Repo for adding and working on new/custom downloaders for Parker Higgins' [xword-dl](https://github.com/thisisparker/xword-dl) crossword downloading tool.

They seem to work but I'm way too uncertain of my Python abilities to contribute upstream yet! May try to fine-tune (mostly to cut out probably-unnecessary code).

Downloaders can currently only fetch "latest" puzzle at the moment, search by date is currently beyond my ability since NYMag/Vulture don't structure either their URLs or AmuseLab puzzle IDs cleanly by date.

Outlets:
    - Vulture's 10x10 pop-culture crossword (released every weekday)
    - New York Magazine's weekly Matt Gaffney joint (released every Sunday)