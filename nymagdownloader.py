import urllib

import dateparser
import requests

from bs4 import BeautifulSoup

from .amuselabsdownloader import AmuseLabsDownloader
from ..util import XWordDLException

class NYMagDownloader(AmuseLabsDownloader):
    command = 'nymag'
    outlet = 'New York Magazine Weekly'
    outlet_prefix = 'NYMag'

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.url_from_id = 'https://cdn3.amuselabs.com/nymag/crossword?id={puzzle_id}&set=nymag-nym weekly'

    @staticmethod
    def matches_url(url_components):
        return ('nymag.com' in url_components.netloc
            and '/crossword/' in url_components.path)

    def find_latest(self):
        index_url = "https://nymag.com/crossword/"
        index_res = requests.get(index_url)
        index_soup = BeautifulSoup(index_res.text, "html.parser")

        latest_url = urllib.parse.urljoin('https://', next(a for a in index_soup.select("a[href^='//nymag.com/article']"))['href'])

        return latest_url

    def find_solver(self, url):
        res = requests.get(url)

        try:
            res.raise_for_status()
        except requests.exceptions.HTTPError:
            raise XWordDLException('Unable to load {}'.format(url))

        soup = BeautifulSoup(res.text, "html.parser")

        iframe_tag = soup.select('iframe[data-crossword-url*="amuselabs.com/nymag/"]')

        iframe_url = str(iframe_tag[0].get('data-crossword-url'))

        try:
            query = urllib.parse.urlparse(iframe_url).query
            query_id = urllib.parse.parse_qs(query)['id']
            self.id = query_id[0]
        except KeyError:
            raise XWordDLException('Cannot find puzzle at {}.'.format(url))

        pubdate_url_component = url.split('/')[-1] or url.split('/')[-2]
        pubdate = pubdate_url_component.replace('-', ' ')
        pubdate_dt = dateparser.parse(pubdate)
        self.date = pubdate_dt

        return super().find_solver(self.find_puzzle_url_from_id(self.id))
